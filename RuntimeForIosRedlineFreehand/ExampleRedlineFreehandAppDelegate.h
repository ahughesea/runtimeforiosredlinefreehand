//
//  ExampleRedlineFreehandAppDelegate.h
//  RuntimeForIosRedlineFreehand
//
//  Created by Esri Australia on 23/10/2014.
//  Copyright (c) 2014 Esri Australia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExampleRedlineFreehandAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
