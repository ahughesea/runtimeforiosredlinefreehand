//
//  main.m
//  RuntimeForIosRedlineFreehand
//
//  Created by Esri Australia on 23/10/2014.
//  Copyright (c) 2014 Esri Australia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ExampleRedlineFreehandAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ExampleRedlineFreehandAppDelegate class]));
    }
}
