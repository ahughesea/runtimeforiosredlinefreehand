//
//  ExampleRedlineFreehandViewController.h
//  RuntimeForIosRedlineFreehand
//
//  Created by Esri Australia on 23/10/2014.
//  Copyright (c) 2014 Esri Australia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ArcGIS/ArcGIS.h>

@interface ExampleRedlineFreehandViewController : UIViewController
@property (strong, nonatomic) IBOutlet AGSMapView *agsMapView;

- (IBAction)toggleRedlineFreehandUISwitchAction:(UISwitch *)sender;

- (IBAction)saveUIButtonAction:(UIButton *)sender;
- (IBAction)clearUIButtonAction:(id)sender;

@end
