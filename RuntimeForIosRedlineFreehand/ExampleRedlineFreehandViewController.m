//
//  ExampleRedlineFreehandViewController.m
//  RuntimeForIosRedlineFreehand
//
//  Created by Esri Australia on 23/10/2014.
//  Copyright (c) 2014 Esri Australia. All rights reserved.
//

#import "ExampleRedlineFreehandViewController.h"

@interface ExampleRedlineFreehandViewController ()

@property (strong, nonatomic) AGSGraphicsLayer *graphicsLayer;
@property (strong, nonatomic) UIView *drawView;
@property (strong, nonatomic) AGSGraphic *drawGraphic;
@property (strong, nonatomic) AGSMutablePolyline *polyline;
@property (assign, nonatomic) CGPoint lastPoint;

@end

@implementation ExampleRedlineFreehandViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Bootstrap - this is not part of the example - start...
    NSURL* url = [NSURL URLWithString:@"http://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer"];
    AGSTiledMapServiceLayer *basemap = [AGSTiledMapServiceLayer tiledMapServiceLayerWithURL:url];
    [self.agsMapView addMapLayer:basemap withName:@"Basemap Tiled Layer"];
    //Bootstrap - this is not part of the example - end.

    
    //Example start, use a graphicsLayer to display what the user's redline as their gesture/drag is captured
    self.graphicsLayer = [[AGSGraphicsLayer alloc] initWithFullEnvelope:nil renderingMode:AGSGraphicsLayerRenderingModeDynamic];
    [self.agsMapView addMapLayer:self.graphicsLayer withName:@"Graphics Layer"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleRedlineFreehandUISwitchAction:(UISwitch *)sender {
    
    if (sender.isOn){
        
        //        [self.toggleRedlineActionUIButton setTitle:@"Stop" forState:UIControlStateNormal];
        
        NSLog(@"Adding the Redline view above the mapView...");
        
        // create a custom view and overlay the mapview
        self.drawView = [[UIView alloc] initWithFrame:self.agsMapView.bounds];
        self.drawView.translatesAutoresizingMaskIntoConstraints = NO;
        self.drawView.backgroundColor = [UIColor clearColor];
        
        // setup the gesture recognizer and add to the agsMapView
        UIPanGestureRecognizer *panGR = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panned:)];
        panGR.maximumNumberOfTouches = 1;
        panGR.minimumNumberOfTouches = 1;
        [self.drawView addGestureRecognizer:panGR];
        [self.agsMapView addSubview:self.drawView];
        
    } else {
        
        NSLog(@"Removing the Redline view from its Superview...");
        //        [self.toggleRedlineFreehandUISwitch setTitle:@"Start" forState:UIControlStateNormal];
        // remove draw view
        [self.drawView removeFromSuperview];
        self.drawView = nil;
        
    }
    
    
}

- (IBAction)toggleRedlineFreehandUISwitchActionBroken:(UISwitch *)sender {
    
    if (sender.isOn){
        
        NSLog(@"Adding the Redline view above the mapView...");
        
        // create a custom view and overlay the mapview
        self.drawView = [[UIView alloc] initWithFrame:self.agsMapView.bounds];
        self.drawView.translatesAutoresizingMaskIntoConstraints = NO;
        self.drawView.backgroundColor = [UIColor clearColor];
        
        // setup the gesture recognizer
        UIPanGestureRecognizer *panGR = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panned:)];
        panGR.maximumNumberOfTouches = 1;
        panGR.minimumNumberOfTouches = 1;
        [self.drawView addGestureRecognizer:panGR];
        [self.view addSubview:self.drawView];
        
        // setup the constraints
        NSDictionary *views = @{@"drawView" : self.drawView};
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[drawView]-250-|" options:0 metrics:nil views:views];
        [self.view addConstraints:constraints];
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[drawView]-0-|" options:0 metrics:nil views:views];
        [self.view addConstraints:constraints];
        

        
    } else {
        
        NSLog(@"Removing the Redline view from its Superview...");

        // remove draw view
        [self.drawView removeFromSuperview];
        self.drawView = nil;
    }
    
}

- (void)panned:(id)sender {
    
    UIPanGestureRecognizer *panGR = (UIPanGestureRecognizer*)sender;
    CGPoint loc = [panGR locationInView:self.drawView];
    
    if (panGR.state == UIGestureRecognizerStateBegan) {
        
        self.lastPoint = loc;
        
        self.polyline = [[AGSMutablePolyline alloc] initWithSpatialReference:self.agsMapView.spatialReference];
        [self.polyline addPathToPolyline];
        
        AGSSimpleLineSymbol *symbol = [AGSSimpleLineSymbol simpleLineSymbolWithColor:[UIColor redColor] width:4.0];
        self.drawGraphic = [AGSGraphic graphicWithGeometry:self.polyline symbol:symbol attributes:nil];
        [self.graphicsLayer addGraphic:self.drawGraphic];
        
    }
    else if (panGR.state == UIGestureRecognizerStateChanged) {
        if (CGPointEqualToPoint(self.lastPoint, loc))
            return;
        self.lastPoint = loc;
        //add the screen point to the polyline using toMapPoint
        [self.polyline addPointToPath:[self.agsMapView toMapPoint:loc]];
        //set this on the graphic geometry so the user can see what they have done
        self.drawGraphic.geometry = self.polyline;
    }
    else if (panGR.state == UIGestureRecognizerStateEnded) {
        self.polyline = nil;
    }
}



- (IBAction)saveUIButtonAction:(UIButton *)sender {
    
    
    if (self.graphicsLayer.graphicsCount > 0){
        
        //This example shows how a redline can be saved to an ArcGIS Feature Service, it assumes that the feature class used to store the (redline) feature has a single (text) attribute called "Notes". This example sets a value for this attribute so it is saved to the server. We can show how this is done, but we wont actually do the save as we have nowhere to save this too.
        
        
        // Union all of the geometries captured on the graphics layer. We do this so we can save/persist as a single (redline) feature
        AGSGeometryEngine *geometryEngine = [AGSGeometryEngine defaultGeometryEngine];
        NSMutableArray *capturedGeometries = [[NSMutableArray alloc] init];
        for(int i=0; i< self.graphicsLayer.graphicsCount; i++){
            [capturedGeometries addObject:[[self.graphicsLayer.graphics objectAtIndex:i] geometry]];
        }
        // Make a new graphic with the difference of the two geometries
        AGSGeometry *capturedGeometry = [geometryEngine unionGeometries:capturedGeometries];
 
        //create the attribute value(s) to be saved with the redline feature
         NSArray *keys = [NSArray arrayWithObjects:@"Notes", nil];
        NSArray *objects = [NSArray arrayWithObjects: @"This is a value that will be saved against a feature attribute called Notes, this value will probably come from a UI Input in reality.", nil];
        NSDictionary *newRedlineAttributes = [NSDictionary dictionaryWithObjects:objects
                                                                         forKeys:keys];
        // Make a new graphic with the union of the geometries and the attributes
        AGSGraphic *newRedlineFeatureToBeSaved = [AGSGraphic graphicWithGeometry:capturedGeometry
                                                          symbol:nil
                                                      attributes:newRedlineAttributes
                                  ];
        
//TODO, here you can save the save the redline using addFeatures. I can't actually        
//        NSOperation *addFeaturesOperation = [self.mapSapnNbnPocRedlineFeatureLayer addFeatures:[NSArray arrayWithObject:newRedlineFeatureToBeSaved]];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Saving, it's Oh So Close.."
                                                        message:@"This demo can't acutally save for real, but the source code contains how this is done but its commented out."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

                              
        
        
        [self.graphicsLayer removeAllGraphics];
        
    } else {
        //todo say sorry there is nothing to save yet
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Nothing to Save Yet"
                                                        message:@"Draw a redline first."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
//        [alert release];
    }
    
}

- (IBAction)clearUIButtonAction:(id)sender {
    [self.graphicsLayer removeAllGraphics];
}
@end
